#ifndef _ASM_ASSERT_H_S_
#define _ASM_ASSERT_H_S_

#ifdef __ASSEMBLER__

;-------------------------------------------------------------------------------
;	The following code reserves space in RAM for use by the debugging
;functions defined in asm_ASSERT.h
;-------------------------------------------------------------------------------
#ifdef NDEBUG
	.print "NDEBUG is defined."
#endif

.ifndef NDEBUG	;Compiles by default. Disabled by defining NDEBUG

	.print "asm_ASSERT_h.S: NDEBUG not defined, assembling debug code."
		.section .data

	$debugtemp0:
		.byte 0x00

	$debugtemp1:
		.byte 0x00

	$debugtemp2:
		.byte 0x00

	$debugtemp3:
		.byte 0x00

	$debugtemp4:
		.byte 0x00

	$debugSREG:
		.byte 0x00

		.section .text
.endif


;-------------------------------------------------------------------------------
;	The following code defines a macro that allows us to save the value of a
;register so that we may later compare it to the register and determine if it
;has been altered.
;PARAMETERS:
;	ZONE - A string, I recommend that you put the name of the current
;function here. Otherwise, this groups together saved registers. Note: For any
;given ZONE+Register combination, you may use this macro once and only once.
;
;	Register - Another string, the name of the register that you wish to
;save.
;-------------------------------------------------------------------------------
.macro DECLARE_Integrity ZONE, Register
	.ifndef NDEBUG
	.print "asm_ASSERT_h.S: Declaring \Register as CRITICAL for group: \ZONE"	
		.section .data
		$\ZONE\Register:
			.byte 0x00
			
		.section .text
		STS \ZONE\Register, \Register
	.endif
.endm


;-------------------------------------------------------------------------------
;	The following code defines a macro that tests values previously saved
;using the "DECLARE_Integrity" macro.
;PARAMETERS:
;	ZONE - A string, I recommend that you put the name of the current
;function here. Otherwise, this groups together saved registers.
;
;	Register - Another string, the name of the register that you wish to
;test.
;-------------------------------------------------------------------------------
.macro ASSERT_Integrity ZONE, Register
	.ifndef NDEBUG
		;save working registers
		STS	debugtemp1, R2
		STS	debugtemp2, R3
		IN	R2, 0x3F ;SREG
		STS	debugSREG, R2

		;compare the register passed with it's former self
		LDS	R2 , \ZONE\()\Register	;the former register
		MOV	R3 , \Register		;the present register
		CPSE	R2 , R3 		;Compare R2 to R3
		BREAK				;Only executed if R2<>R3

		LDS	R2 , debugSREG
		OUT	0x3F , R2		;SREG
		LDS	R2 , debugtemp1
		LDS	R3 , debugtemp2
	.endif
.endm

#endif /* __ASSEMBLER__ */

#endif
