[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

# Simplicity RTOS (Real Time Operating System)
This is a multitasking system kernel for the [Atmel AVR](https://www.microchip.com/) architecture.
It was written for the AT89C52 micro controller which appears to have been superseded by the AT89S52.
For whatever it's worth, this was done before Arduino came along and made such projects more
approachable for beginners.

## Vision
* A minimalist kernel that runs on minimal hardware
* Usable without compiling your code into it - '''otherwise you'd need to contribute your code back to the project'''

## Getting Started
When this was originally written a development environment called Atmel Studio was being used
(although it may have been called AVR Studio at the time). The contents of `main.c` test the kernel
with four "concurrently" running threads. Since the kernel currently lacks a proper scheduler,
they'll all get equal processor time.

### Prerequisites
* Retargetable Compiler
* A [chip programmer](https://www.microchip.com/DevelopmentTools/ProductDetails/ATAVRDRAGON) - I put
  a "zif socket" on mine. It makes getting the chips in and out much easier.

### Using
To do work with this kernel initialize threads in the main function of `main.c`.
