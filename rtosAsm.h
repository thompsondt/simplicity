

#define _elementPtr_L		0
#define _elementPtr_H		1
#define _auxInfo			2
#define _nextListNodePtr_L		3
#define _nextListNodePtr_H		4




#define MUTEX_Mutex			0
#define MUTEX_waitList			1

#define MUTEX__elementPtr_L					\
	MUTEX_waitList+_elementPtr_L

#define MUTEX__elementPtr_H					\
	MUTEX_waitList+_elementPtr_H

#define MUTEX__auxInfo					\
	MUTEX_waitList+_auxInfo

#define MUTEX__nextListNodePtr_L				\
	MUTEX_waitList+_nextListNodePtr_L

#define MUTEX__nextListNodePtr_H				\
	MUTEX_waitList+_nextListNodePtr_H






#define TCB_R2		0
#define TCB_R3		1
#define TCB_R4		2
#define TCB_R5		3
#define TCB_R6		4
#define TCB_R7		5
#define TCB_R8		6
#define TCB_R9		7
#define TCB_R10		8
#define TCB_R11		9
#define TCB_R12		10
#define TCB_R13		11
#define TCB_R14		12
#define TCB_R15		13
#define TCB_R16		14
#define TCB_R17		15

#define TCB_R18		16
#define TCB_R19		17
#define TCB_R20		18
#define TCB_R21		19
#define TCB_R22		20
#define TCB_R23		21
#define TCB_R24		22
#define TCB_R25		23

#define TCB_R28		24
#define TCB_R29		25

#define TCB_CPSR		26
#define TCB_SP_L		27
#define TCB_SP_H		28
#define TCB_RV_L		29
#define TCB_RV_H		30
#define	TCB_priority	31
#define	TCB_WLNODE				32
#define TCB_WaitListNODEPtr_L		32
#define TCB_WaitListNODEPtr_H		33
#define TCB_TLNODE			34
#define	TCB_TimerListNODEPtr_L		34
#define TCB_TimerListNODEPtr_H		35
#define TCB_threadObjectNamePtr_L	36
#define TCB_threadObjectNamePtr_H	37


#define TCB_TLNODE_nextListNodePtr_L			\
	TCB_TLNODE+_nextListNodePtr_L

#define TCB_TLNODE_nextListNodePtr_H			\
	TCB_TLNODE+_nextListNodePtr_H
