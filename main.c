#include <avr/io.h>
#include <avr/interrupt.h>
#include "rtos.h"

void main (void)
{
rtosInitAsm();
threadObject_t	thread1;
threadObject_t	thread2;
threadObject_t	thread3;
threadObject_t	thread4;




mutexObject_t	myMutex;
mutexObjectInit(&myMutex, 0);


timerListTest();



FORCE_SET_RUNNING_THREAD(&thread1);
mutexObjectLock(&myMutex, 100);

FORCE_SET_RUNNING_THREAD(&thread2);
mutexObjectLock(&myMutex, 150);

FORCE_SET_RUNNING_THREAD(&thread3);
mutexObjectLock(&myMutex, 25);

FORCE_SET_RUNNING_THREAD(&thread4);
mutexObjectLock(&myMutex, 100);

	//end of program
main();
}

