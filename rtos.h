#ifndef _RTOS_H_
#define _RTOS_H_

//Workaround to make sure that threadObject_t has *listObject_t inside it,
//and that listObject_t has *threatObject_t inside it.
struct _threadObject_;
struct _listObject_;

///List Node Structure
typedef struct _listObject_
{
	struct _threadObject_ *element; // OR the bottom node...
	char	auxInfo;
	struct _listObject_ *nextListNode;
} listNode_t;

///List Object
typedef listNode_t listObject_t;

///Thread Control Block (context)
typedef struct _threadObject_
{
	char		R2,
			R3,
			R4,
			R5,
			R6,
			R7,
			R8,
			R9,
			R10,
			R11,
			R12,
			R13,
			R14,
			R15,
			R16,
			R17,

			R18,		//Optional
			R19,		//Optional
			R20,		//Optional
			R21,		//Optional
			R22,		//Optional
			R23,		//Optional
			R24,		//Optional
			R25,		//Optional

			R28,
			R29;		///<General working registers.

	char		cpsr;		///<Status Register
	char		SP_L;		///<Stack Pointer Low Byte
	char		SP_H;		///<Stack Pointer High Byte
	char		ResumeVia_L;	///<Resume Via Address Low Byte
	char		ResumeVia_H;	///<Resume Via Address High Byte
	char		priority;	///<Priority.
	
	listNode_t
			waitListNODE;	///<Wait List

	listNode_t
			timerListNODE; ///<Wait List Timer

	char		*threadObjectName;
					///<Character string, for debugging.
	
} threadObject_t;


///Mutex Object
typedef struct
{
	char		Mutex;
	listObject_t	waitList;
}mutexObject_t;

///Semaphore Object
typedef struct
{
	char			count;
	listObject_t	waitList;
}semaphoreObject_t;

///Mailbox Packet Structure
typedef struct
{
	char			*mailboxBuffer;
	char			readIndex;
	char			writeIndex;
	char			mailboxBufferSize;
	char			emptyBufferSize;
	char			messageSize;
	listObject_t	waitList;
} mailboxObject_t;


/*---------------------------------------------------------------------------*/


listNode_t *listNodeAlloc(void);

void listNodeFree (listNode_t *listNodePtr);

void listObjectModuleInit(void);
void listObjectInit(listObject_t *listObjectPtr);

void listObjectInsert
(
	listObject_t *listNodePtr, 
	threadObject_t *newThreadObject
);

threadObject_t *listObjectDelete(listObject_t *listObjectPtr);

void listObjectDeleteMiddle
(
	listObject_t *waitList,
	threadObject_t *threadObjectToBeDeleted
);

char listObjectCount(listObject_t *listObjectPtr);

void threadObjectCreate
(
	threadObject_t	*threadObjectPtr,
	void			(*FP)(),
	char			arg1,
	char			arg2,
	char			arg3,
	char			arg4,
	char			*stackPointer,
	char			priority,
	char			cpsr,
	char			*threadObjectName
);

void threadObjectDestroy(threadObject_t *threadObjectPtr);

void mutexObjectInit(mutexObject_t *mutexObjectPtr, char initialFlag);

char mutexObjectLock(mutexObject_t *mutexObjectPtr, char waitTime) /*__attribute__((naked))*/;

void mutexObjectRelease(mutexObject_t *mutexObjectPtr);

void semaphoreObjectInit(semaphoreObject_t *semaphoreObjectPtr, char initialCount);

char semaphoreObjectPend(semaphoreObject_t *semaphoreObjectPtr);

void semaphoreObjectPost(semaphoreObject_t *semaphoreObjectPtr);

void mailboxObjectInit
(
	mailboxObject_t	*mailboxObjectPtr,
	char			*mailboxBuffer,
	char			mailboxBufferSize,
	char			messageSize
);

char mailboxObjectPend
(
	mailboxObject_t *mailboxObjectPtr,
	char			waitFlag,
	void			*message
);

char mailboxObjectPost
(
	mailboxObject_t *mailboxObjectPtr,
	char			waitFlag,
	void			*message
);



void scheduler(void);
void rtosInit(void);
void block(void);
void timerTick(void);
void sleep(char TickCount);

#endif /* _RTOS_H_ */
