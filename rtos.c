#include <avr/io.h>
#include <avr/interrupt.h>
#include "rtos.h"

/*----------------------------------------------------------------------------*/
//These are book keeping variables for the kernel.
	short			kStack[16];
	threadObject_t	*runningThreadPtr;
	listObject_t	readyList;
	listObject_t	timerList;
	//long long int time;
	threadObject_t	idleThread;
	//long int		idleStack[5];

	listObject_t node1, node2, node3, node4, node5;

void insert_runningThread_IntoTimerList (char waitTime)
	__attribute__((naked,hot,used));

extern void timerListTest(void);


extern void rtosInitAsm(void);
//extern void interrupt_disable(void);
//extern void interrupt_restore(void);


void timerListTest()
{
	//assemble test nodes for timerLists
	timerList.auxInfo = 3;
	timerList.nextListNode = &node1;

	node1.auxInfo = 25;
	node1.nextListNode = &node2;

	node2.auxInfo = 75;
	node2.nextListNode = &node3;

	node3.auxInfo = 50;

	insert_runningThread_IntoTimerList (255);
	
	return;
}

extern void FORCE_SET_RUNNING_THREAD(threadObject_t *newRunningThread);


void rtosInitAsm(void)
{
	return;
}


void FORCE_SET_RUNNING_THREAD(threadObject_t *newRunningThread)
{
	runningThreadPtr = newRunningThread;
	return;
}

/*----------------------------------------------------------------------------*/
//listObject_t Function Definitions (defined in rtos.h)	
void listObjectModuleInit(void)
{

	return;
}

listNode_t *listNodeAlloc(void)
{

}

void listNodeFree(listNode_t *listNodePtr)
{

}

void listObjectInit(listObject_t *listObjectPtr)
{

}

void listObjectInsert
	(listObject_t *listNodePtr, threadObject_t *newThreadPtr)
{

}


threadObject_t *listObjectDelete(listObject_t *listObjectPtr)
{

}

void listObjectDeleteMiddle
	(listObject_t *waitList, threadObject_t *threadObjectToBeDeleted)
{

}

char listObjectCount(listObject_t *listObjectPtr)
{

}

/*----------------------------------------------------------------------------*/

void idleFunction(void)
{

}

/*----------------------------------------------------------------------------*/
//RTOS Function Definitions (Declared in rtos.h)

void rtosInit(void)
{

}

char is_thread_switch_needed(void)
{

}

/*----------------------------------------------------------------------------*/
//Timer Functions

void insert_runningThread_IntoTimerList (char waitTime)
{
	listNode_t *activeNode = &timerList;
	char nodeCount = 0;

	//if the list is empty, then insert the first node and return.
	if (timerList.auxInfo == 0)
	{
	}
	
	/* == DECEND PAST LOWER == */
	/*get the first node, then decend past all nodes with a lower waitTime.
	Also, process four links at a time.
	*/

	while(activeNode->nextListNode->auxInfo < waitTime && nodeCount++ < timerList.auxInfo)
	{
	//dave dave dave
		activeNode = activeNode->nextListNode; //go to next node.

		waitTime -= activeNode->auxInfo;
	}

	/* == INSERT NODE == */
	runningThreadPtr->timerListNODE.nextListNode = activeNode->nextListNode;
	activeNode->nextListNode = &runningThreadPtr->timerListNODE;

	/* == UPDATE REMAINING == */
	if (nodeCount == timerList.auxInfo)
	{
		timerList.auxInfo++;
		return;
	}

	activeNode = &runningThreadPtr->timerListNODE;
	
	nodeCount--;
	while(nodeCount++ < timerList.auxInfo)
	{
		activeNode = activeNode->nextListNode; //go to next node.
		if(activeNode->auxInfo > 0)
		{
			activeNode->auxInfo -= waitTime;	//subtract remaining time.
		}
	}

	timerList.auxInfo = nodeCount;
}

void insertIntoTimerList
	(threadObject_t *newThreadObject, listObject_t *waitList)
{
	
}

void deleteFromTimerList(threadObject_t *threadObjectToBeDeleted)
{

}

void timerTick(void)
{

}

/*----------------------------------------------------------------------------*/

void threadObjectDestroy(threadObject_t *threadObjectPtr)
{

}

/*----------------------------------------------------------------------------*/

void mailboxObjectInit
	(
		mailboxObject_t	*mailboxObjectPtr,
		char			*mailboxBuffer,
		char			mailboxBufferSize,
		char			messageSize
	)
{

}








void mutexObjectInit(mutexObject_t *mutexObjectPtr, char initialFlag)
{
	mutexObjectPtr->Mutex = initialFlag;
	mutexObjectPtr->waitList.element = 0;
	mutexObjectPtr->waitList.auxInfo = 1; //indicates that waitList is empty.
	mutexObjectPtr->waitList.nextListNode = 0;

	return;
}





void semaphoreObjectInit
	(
		semaphoreObject_t *semaphoreObjectPtr,
		char initialCount
	)
{

}
