
/*
 * Global register variables.
 */
#ifdef __ASSEMBLER__

	//assembly declarations can go in here.

.global mutexObjectInit
.global mutexObjectLock
.global mutexObjectRelease

#else  /* !ASSEMBLER */





extern char mutexObjectLock (char mutexObjectPtr, char waitFlag);
extern char mutexObjectInit (char mutexObjectPtr, char waitFlag);
extern char mutexObjectRelease (char mutexObjectPtr, char waitFlag);






#endif /* ASSEMBLER */
